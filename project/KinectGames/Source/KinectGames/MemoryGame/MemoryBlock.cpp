// Fill out your copyright notice in the Description page of Project Settings.

#include "KinectGames.h"
#include "MemoryBlock.h"


// Sets default values
AMemoryBlock::AMemoryBlock() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//ConstructorHelpers::FObjectFinderOptional<UStaticMesh> blockMesh(TEXT("/Game/Assets/CardMesh.CardMesh"));
	//blockMeshComponent->SetStaticMesh(blockMesh.Get());

	dummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("dummyRoot"));
	RootComponent = dummyRoot;
	blockMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("blockMesh"));
	blockMeshComponent->SetupAttachment(dummyRoot);
}

// Called when the game starts or when spawned
void AMemoryBlock::BeginPlay() {
	Super::BeginPlay();
}

// Called every frame
void AMemoryBlock::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

}
