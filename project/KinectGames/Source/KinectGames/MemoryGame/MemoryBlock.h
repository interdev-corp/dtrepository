// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "MemoryBlock.generated.h"

UCLASS()
class KINECTGAMES_API AMemoryBlock : public AActor {
	GENERATED_BODY()

	public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = MemoryBlock)
	USceneComponent* dummyRoot;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = MemoryBlock)
	class UStaticMeshComponent* blockMeshComponent;

	// Sets default values for this actor's properties
	AMemoryBlock();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaSeconds) override;


};
