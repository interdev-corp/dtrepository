// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "FBlockDescriptor.h"
#include "Grid.generated.h"

UCLASS()
class KINECTGAMES_API AGrid : public AActor {

	GENERATED_BODY()

	public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Grid)
	USceneComponent* dummyRoot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Grid)
	int32 width;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Grid)
	int32 height;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Grid)
	float spacing;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Grid)
	TSubclassOf<AActor> blockType;

	// Sets default values for this actor's properties
	AGrid();

	// Called when the game starts or when spawned
	void BeginPlay() override;

	// Called every frame
	void Tick(float DeltaSeconds) override;

	UFUNCTION(BlueprintCallable, Category = Grid)
	void populateGrid();

	private:
	TArray<TArray<FBlockDescriptor>> gridData;
};
