#pragma once

#include "KinectGames.h"
#include "FBlockDescriptor.generated.h"

USTRUCT()
struct FBlockDescriptor {

	GENERATED_BODY()

	UPROPERTY()
	int32 id;
	UPROPERTY()
	AActor* blockActor;

};