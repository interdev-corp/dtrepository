// Fill out your copyright notice in the Description page of Project Settings.

#include "KinectGames.h"
#include "Grid.h"

// Sets default values
AGrid::AGrid() {
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	dummyRoot = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	RootComponent = dummyRoot;
}

// Called when the game starts or when spawned
void AGrid::BeginPlay() {
	Super::BeginPlay();
}

// Called every frame
void AGrid::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);
}

void AGrid::populateGrid() {
	const FVector startLocation = GetActorLocation();
	for (int32 h = 0; h < height; h++) {
		TArray<FBlockDescriptor> gridRow;
		for (int32 w = 0; w < width; w++) {
			const FVector position = startLocation + FVector(w * spacing, h * spacing, 0.0f);
			AActor* newBlock = GetWorld()->SpawnActor<AActor>(blockType, position, FRotator(0, 0, 0));
			//gridRow.Add({ 1, newBlock });
		}
		gridData.Add(gridRow);
	}
}
