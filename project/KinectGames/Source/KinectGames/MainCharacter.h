// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "MainCharacter.generated.h"

UCLASS()

class KINECTGAMES_API AMainCharacter : public ACharacter {
	GENERATED_BODY()

	public:
	// Sets default values for this character's properties
	AMainCharacter();

	// Called when the game starts or when spawned
	void BeginPlay() override;

	// Called every frame
	void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UFUNCTION(BlueprintCallable, Category="Movement")
	void MoveForward(float Val);

	UFUNCTION()
	void MoveRight(float Val);

};
