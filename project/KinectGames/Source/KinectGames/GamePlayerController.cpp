// Fill out your copyright notice in the Description page of Project Settings.

#include "KinectGames.h"
#include "GamePlayerController.h"
#include <Windows.h>

AGamePlayerController::AGamePlayerController() : Super() {
}

void AGamePlayerController::invokeMouseClick() {
	SWindow* win = FSlateApplication::Get().GetActiveTopLevelWindow().Get();
	if(win) {
		FSlateApplication::Get().OnMouseDown(win->GetNativeWindow(), EMouseButtons::Left);
		FSlateApplication::Get().OnMouseUp(EMouseButtons::Left);
	}
}
