// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "GamePlayerController.generated.h"

/**
 * 
 */
UCLASS()
class KINECTGAMES_API AGamePlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	AGamePlayerController();

	UFUNCTION(BlueprintCallable, Category = "Kinect Games UI")
	void invokeMouseClick();
	
};
