// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/GameMode.h"
#include "KinectGamesGameMode.generated.h"

/**
 * 
 */
UCLASS()
class KINECTGAMES_API AKinectGamesGameMode : public AGameMode {

	GENERATED_BODY()

private:

	UPROPERTY()
	APlayerController* lastPlayerController;

	/*
	* The widget instance that we are using as our menu.
	*/
	UPROPERTY()
	UUserWidget* currentWidget;

public:

	AKinectGamesGameMode();
	
	void StartPlay() override;

	// Properties

	/* 
	 * The widget class we will use as our menu when the game starts. 
	 */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Kinect Games UI")
	TSubclassOf<UUserWidget> startingWidgetClass;

	// Functions

	/*
	 * Remove the current menu widget and create a new one from the specified class, if provided. 
	 */
	UFUNCTION(BlueprintCallable, Category = "Kinect Games UI")
	void setOrReplaceWidget(TSubclassOf<UUserWidget> newWidgetClass);

	UFUNCTION(BlueprintCallable, Category = "Kinect Games UI")
	void showCursor(bool show = true);

	UFUNCTION(BlueprintCallable, Category = "Kinect Games UI")
	void setMousePosition(int32 x, int32 y);

};
