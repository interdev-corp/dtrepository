// Fill out your copyright notice in the Description page of Project Settings.

#include "KinectGames.h"
#include "KinectGamesGameMode.h"
#include "MainCharacter.h"
#include "GamePlayerController.h"

AKinectGamesGameMode::AKinectGamesGameMode() : Super() {
	DefaultPawnClass = AMainCharacter::StaticClass();
	PlayerControllerClass = AGamePlayerController::StaticClass();
}

void AKinectGamesGameMode::StartPlay() {
	Super::StartPlay();

	if(GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("Default Game Mode Activated! Yohoo"));
	}
}

void AKinectGamesGameMode::setOrReplaceWidget(TSubclassOf<UUserWidget> newWidgetClass) {
	if(currentWidget != nullptr) {
		currentWidget->RemoveFromViewport();
		currentWidget = nullptr;
	}
	if(newWidgetClass != nullptr) {
		currentWidget = CreateWidget<UUserWidget>(GetWorld(), newWidgetClass);
		if(currentWidget != nullptr) {
			currentWidget->AddToPlayerScreen();
		}
	}
}

void AKinectGamesGameMode::showCursor(bool show) {
	lastPlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if(lastPlayerController != nullptr) {
		lastPlayerController->bShowMouseCursor = show;
		lastPlayerController->bLockLocation = true;
        GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT("Cursor Showed"));
	}
}

void AKinectGamesGameMode::setMousePosition(int32 x, int32 y) {
	FViewport* v = CastChecked<ULocalPlayer>(UGameplayStatics::GetPlayerController(GetWorld(), 0)->GetLocalPlayer())->ViewportClient->Viewport;
	v->SetMouse(x, y);
}
